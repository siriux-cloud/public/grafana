ARG GRAFANA_VERSION=9.5.2

# Agregamos Step Cli para iteractuar con Step CA
FROM smallstep/step-cli as step

# Agregamos Grafana Version
FROM grafana/grafana:${GRAFANA_VERSION}

# Rooteamos
USER root

# Instalar step
COPY --from=step /usr/local/bin/step /usr/local/bin/

# Instalamos gomplate para manipular templates
ARG GOMPLATE_VERSION="3.11.5"
RUN wget https://github.com/hairyhenderson/gomplate/releases/download/v${GOMPLATE_VERSION}/gomplate_linux-amd64 -O /usr/bin/gomplate && \
    chmod +x /usr/bin/gomplate

ARG YQ_VERSION="4.35.2"
RUN wget https://github.com/mikefarah/yq/releases/download/v${YQ_VERSION}/yq_linux_amd64 -O /usr/bin/yq && \
    chmod +x /usr/bin/yq

# Copiamos archivos del usuasrio root
COPY root/ /

USER grafana
